# Dino Game



## Sommaire

[Présentation du projet](#présentation-du-projet)
1. [Technologies utilisées](#1-technologies-utilisées)
2. [Interets](#2-intérets)
3. [Installation et Utilisation](#3-installation-et-utilisation)
4. [Diagrammes](#4-diagrammes)
    * Use Case
    * Class
5. [Points forts](#5-points-forts)
6. [La v2](#6-la-v2)
7. [Aperçu en image](#7-aperçu)
8. [Exemple de code utilisé](#8-exemple-de-code-utilisé)
9. [Difficultés](#9-difficultés)
10. [Ce que j'ai apprécié](#10-ce-que-jai-apprécié)
11. [License](#11-license)



### <span name="présentation-du-projet">Présentation du projet</span>

>Dino Game est un jeu de bataille de dinosaure.<br>
C'était notre premier projet en JavaScript individuel.<br>
Nous devions créer le jeu de notre choix.


## <span name="1-technologies-utilisées">1. Technologies utilisées</span>

* HTML
* CSS
* JavaScript



## <span name="2-intérets">2. Intérets</span>

Le principal intéret était de nous former au JavaScript en appliquant les méthodes apprises en cours.



## <span name="3-installation-et-utilisation">3. Installation et utilisation</span>

1. Cloner le projet
2. Enjoy !
  * Flèche droite pour attaquer.
  * Espace pour effectuer une "Super Attack"(seulement quand le panneau "SuperAttack" apparait).
  >Attention, votre adversaire vous attaque aussi et peu esquiver vos coups(vous esquiverez les siens aléatoirement).
  * Le vélociraptor est le plus rapide, il attaque toujours en premier. 



## <span name="4-diagrammes">4. Diagrammes</span>

  * __Use Case__

Le diagramme n'est pas complet(super attaque manquante).

<img src="UML/use_case_dino_game.jpg">

  * __Diagramme des classes__

Le diagramme n'est pas complet(super attaque manquante).

<img src="UML/class_dino_game.jpg">



## <span name="5-points-forts">5. Points forts</span>

* Dynamique 



## <span name="6-la-v2">6. La v2</span>

* Plus de précision et d'ordre.
* Des scores
* +de possibilités



## <span name="7-aperçu">7. Aperçu</span>

<img src="images/dino-game.png">



## <span name="8-exemple-de-code-utilisé">8. Exemple de code utilisé</span>

*Extrait de la classe*
```javascript
class Dino {
    /**
     * @param {Array} simpleAttack 
     * @param {Array} superAttack 
     * @param {HTMLElement} imagePerso
     * @param {HTMLElement} imageRival 
     */
    constructor(simpleAttack, superAttack, imagePerso, imageRival) {
        this.simpleAttack = simpleAttack;
        this.superAttack = superAttack;
        this.imagePerso = imagePerso;
        this.imageRival = imageRival;
    }

    // ...
```

*Extrait de méthodes de la classe*
```javascript
    // Super attaque du rival
    maxiAttackRival(power){
      let result = this.superAttack[Math.floor(Math.random()*this.superAttack.length)];
      power.value -= result; 

      message();
      displayNone();  
    }
    // Attaque du vélociraptor
    veloAttack(power){
      power.value -= this.superAttack[0];
    }
```



## <span name="9-difficultés">9. Difficultés</span>

Etant mon premier "gros" projet en JavaScript, je me suis vite retrouvée submerger par trop de données(plusieurs dinosaures + leurs attaques etc...).
Peut-être ne laisser que 3 dinosaures puis performer le reste :).



## <span name="10-ce-que-jai-apprécié">10. Ce que j'ai apprécié</span>

Manipuler le DOM, mettre en mouvement les éléments.



## <span name="11-license">11. License</span>

Le projet et son code peuvent être utilisés sans limitations.